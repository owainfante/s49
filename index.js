// Get Post Data
fetch("https://jsonplaceholder.typicode.com/posts")
.then(res => res.json())
.then(result => showPost(result))

const showPost = (posts) => {

	let postEntries = ''
	posts.forEach((post) => {

		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
			})
	document.querySelector('#div-post-entries').innerHTML = postEntries
}

// Add New Post
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
	e.preventDefault()

	fetch("https://jsonplaceholder.typicode.com/posts", {
		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		}),
		headers: {
			'Content-Type': 'application/json'
		}
	})
	.then(res => res.json())
	.then(result => {
		console.log(result)
		alert('Post Successfully Created!')

		// This clears the input fields after the data is successfully added
		document.querySelector('#txt-title').value = null
		document.querySelector('#txt-body').value = null
	})
})

// Edit Post
const editPost = (postId) => {
	let title = document.querySelector(`#post-title-${postId}`).innerHTML
	let body = document.querySelector(`#post-body-${postId}`).innerHTML

	document.querySelector('#txt-edit-title').value = title
	document.querySelector('#txt-edit-body').value = body
	document.querySelector('#txt-edit-id').value = postId
	document.querySelector('#btn-submit-update').removeAttribute('disabled')
}


// Update
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault()

	fetch("https://jsonplaceholder.typicode.com/posts/1", {
		method: 'PUT',
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userID: 1
		}),
		headers: {
			'Content-Type': 'application/json'
		}
	})
	.then(res => res.json())
	.then(result => {
		console.log(result)
		alert('Post Successfully Updated!')

		document.querySelector('#txt-edit-id').value = null
		document.querySelector('#txt-edit-title').value = null
		document.querySelector('#txt-edit-body').value = null
		document.querySelector('#btn-submit-update').setAttribute('disabled', true)
	})
})

// Activity - Refactor the deletePost function to be able to delete an existing post from the API. The API should return a response containing the post that you just deleted. The output should be in the console logging the post that was just deleted.

const deletePost = (id) => {

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
	    method: 'DELETE',
	})
	.then(res => res.json())
    .then(result => {
    	console.log(result)
		alert('Post successfully deleted!')
		
		document.querySelector(`#post-${id}`).remove()
	})
}